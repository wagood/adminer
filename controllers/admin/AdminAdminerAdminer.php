<?php 
/******************************************************
 *  Adminer Database management
 *
 * @package   adminer
 * @version   0.1
 * @author    Wagood (wagood@yandex.ru)
 * 
 * ******************************************************/

class AdminAdminerAdminerController extends ModuleAdminController 
{ 		
	public function __construct()
	{
		$this->bootstrap = true;
		$this->display = 'view';
		$this->addRowAction('view');
		parent::__construct();					 
	}

	public function setMedia()
	{
		parent::setMedia();
	}
		 
	public function renderView()
	{
	 	$link = $this->context->link;

		$template = $this->createTemplate('iframe.tpl');

		$template->assign( array(
			'path' => '/modules/' . $this->module->name . '/lib/index.php?token='.$token = Tools::getAdminToken('AdminAdminerAdminer'.intval(Tab::getIdFromClassName('AdminAdminer')).intval($this->context->employee->id)),
		));
		
		return $template->fetch();
	}		 

}
?>