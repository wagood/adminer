<?php
/******************************************************
 *  Adminer Database management
 *
 * @package   adminer
 * @version   0.1
 * @author    Wagood (wagood@yandex.ru)
 *
 * ******************************************************/

require(dirname(__FILE__).'./../../../config/config.inc.php');
$cookie = new Cookie('psAdmin'); // Use "psAdmin" to read an employee's cookie.
$context = Context::getContext();
$token = Tools::getAdminToken('AdminAdminerAdminer'.intval(Tab::getIdFromClassName('AdminAdminer')).intval($cookie->id_employee));

// try to load language
$employee = new Employee (intval($cookie->id_employee));
/* Try to set language */
if (Validate::isLoadedObject($employee))
{
	$language = new Language (intval($employee->id_lang));
	if (Validate::isLoadedObject($language))
		$language = $language->iso_code; 
	
	if (isset($language) && isset($_COOKIE["adminer_lang"]) && $_COOKIE["adminer_lang"] <> $language) {
		$_SERVER["HTTP_ACCEPT_LANGUAGE"] = $_SESSION["lang"] = $_COOKIE["adminer_lang"] = $_COOKIE["adminer_lang_my"] = $language;
		$_SESSION["translations"] = array();	
		$LANG = $language;		
	}
}

if (isset($_GET['token']) && $token == $_GET['token']) {
	$_GET['username'] = _DB_USER_;	
}

function adminer_object() {
	
	global $LANG;
	
	class AdminerSoftware extends Adminer {

	function AdminerFrames($sameOrigin = false) {
		$this->sameOrigin = $sameOrigin;
	}
	
	function get_lang() {
		global $LANG;
		if (isset($_COOKIE["adminer_lang_my"])) $LANG = $_COOKIE["adminer_lang_my"];
		$_GET['lang'] = $LANG;
		return $LANG;
	}
	
	function headers() {
		if ($this->sameOrigin) {
			header("X-Frame-Options: SameOrigin");
		}
		header("X-XSS-Protection: 0");
		return false;
	}
	
		function name() {
				return 'Adminer';
		}

		function permanentLogin() {
			// key used for permanent login
			return "74b941992ef29727ccabf82889fe837a";
		}

		function credentials() {
			// server, username and password for connecting to database
			return array(
				_DB_SERVER_, _DB_USER_, _DB_PASSWD_
			);
		}

		function database() {
			// database name, will be escaped by Adminer
			return _DB_NAME_;
		}

		/** Print homepage
		 * @return bool whether to print default homepage
		 */
		function homepage() {
			echo '<p><form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="B7J6TWADFG53Y"><input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online."><img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1"></form></p>';
			echo '<p class="tabs">' . ($_GET["ns"] == "" ? '<a href="' . h(ME) . 'database=">' . lang('Alter database') . "</a>\n" : "");
			if (support("scheme")) {
				echo"<a href='" . h(ME) . "scheme='>" . ($_GET["ns"] != "" ? lang('Alter schema') : lang('Create schema')) .
				"</a>\n";
			}
			return true;
		}

		function head ()
		{
			echo "<script>window.onload = alertParent;function alertParent () {parent.alertsize(document.body.scrollWidth, document.body.scrollHeight);}</script><style>.logout, #dbs {visibility: hidden;}</style>";
			/*if (isset($_COOKIE["adminer_lang_my"]))		
				echo "<style>#lang {visibility: hidden;}</style>";*/			
		}
	}
	return new AdminerSoftware($plugins);
}

if (Validate::isLoadedObject($employee)) 
{
	$_GET['db'] = _DB_NAME_;
	include './adminer-mysql.php';
}

