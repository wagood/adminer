<?php
/******************************************************
 *  Adminer Database management
 *
 * @package   adminer
 * @version   0.1
 * @author    Wagood (wagood@yandex.ru)
 * 
 * ******************************************************/

if (!defined('_PS_VERSION_'))
    exit;

class Adminer extends Module {

    /**
     * Constructor
     */
    public function __construct() {

        global $currentIndex;
           
        $this->name = 'adminer';
        $this->tab = 'administration';
        $this->version = '0.1';
        $this->author = 'Wagood';
        $this->need_instance = 0;
        $this->is_configurable = 0;
        $this->bootstrap = true;
       
        parent::__construct();
 
        $this->displayName = $this->l('Adminer');
        $this->description = $this->l('Adminer Database management');
    }
    
    private function installModuleTab($title, $sfx = '', $parent = '') 
    {
    	@copy(_PS_MODULE_DIR_ . $this->name . '/logo.png', _PS_IMG_DIR_ . 't/' . $class . '.png');
    	if ($parent == '') {
    		$position = Tab::getCurrentTabId();
    	} else {
    		$position = Tab::getIdFromClassName($parent);
    	}
    
    	$tab = new Tab();
    	$tab->class_name = 'Admin' . ucfirst($this->name) . $sfx;
    	$tab->module = $this->name;
    	$tab->id_parent = intval($position);
    	$langs = Language::getLanguages(false);
    	foreach ($langs as $l) {
    		$tab->name[$l['id_lang']] = $title;
    	}
    	$id_tab = $tab->add(true, false);
    	return true;
    }
    
    /**
     * Uninstall
     */
    private function uninstallModuleTab($sfx = '') 
    {    	    
    	$idTab = Tab::getIdFromClassName('Admin' . ucfirst($this->name) . $sfx);
    	if ($idTab != 0) {
    		$tab = new Tab($idTab);
    		$tab->delete();
    		return true;
    	}
    	return false;
    }
       
    /**
     * @see Module::install()
     */
    public function install() 
    {           
        if (parent::install()  
        		&& $this->registerHook('displayBackOfficeHeader')
        		&& $this->installModuleTab('Database Management', 'Adminer', 'AdminAdmin')
        	) 
			return true;

        return false;
    }
 
    public function hookDisplayBackOfficeHeader()
    {  
    	//$this->context->controller->addCss($this->_path.'assets/admin/css/adminer.css');
        $this->context->controller->addJs($this->_path.'assets/admin/js/adminer.js');
    }
    
     /**
     * @see Module::uninstall()
     */
    public function uninstall() {
 
        if ( parent::uninstall() ) {
            $this->uninstallModuleTab("Adminer");
            return true;
        }
        return false;
    }
        
}